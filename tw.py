__author__ = 'sandro.lourenco'


import utils
import argparse

STOPWORDS = dict([('WOODCHUCK', 0),
                  ('IS', 1),
                  ('HOW MANY', 2),
                  ('CREDITS', 3),
                  ('?', 4)])

GALATICCURRENCY = ['GLOB', 'PROK', 'PISH', 'TEGJ']

METALS = ['SILVER', 'GOLD', 'IRON']


class UnknownMaterial(Exception):
    pass


class UnknownMetal(Exception):
    pass


class MGG(object):
    """
    class to calculate currencies of the metals from the galaxy
    """

    def __init__(self, filein, verboseo):
        """

        :param filein: path of the file with lines to process
        :param verboseo: prints output
        :return: None
        """
        self.gcurrency = {}
        self.mprice = {}
        self.verboseo = verboseo
        self.filein = open(filein, 'r')

    def run(self):
        """
        get the provided text and splits by line and classify each entry
        :param text: unicode text
        :return: dict of processed text
        """
        qw = []

        for line in self.filein.readlines():
            try:
                data = self.type_and_action(line)
                for action in data['type']:
                    if action == 1:
                        cc, cw = self.conversion(data['words'])
                    elif (action == 2 and data['hmetal'] is True and data['isq'] is False) or action == 3:
                        mc, mw = self.metal_price(data['words'], cc)
                    elif action == 4:
                        if 'CREDITS' in data['words']:
                            qw.append('{} {} is {} Credits'.format(' '.join(cw).lower(), mw.capitalize(), cc + mc))
                        else:
                            qw.append('{} is {}'.format(' '.join(cw).lower(), cc))

            except UnknownMaterial as e:
                qw.append(e.__str__())

            except Exception as e:
                print('General unknown error: {}'.format(e))

        if self.verboseo:
            print(qw)
        return qw

    def type_and_action(self, line):
        """
        Defines what type of action is needed from the text line
        :param line: text line
        :return: type, items
        """
        line = line.upper().rstrip()
        words = line.split(' ')
        words_qtd = len(words)
        has_curr = True if set(words).intersection(GALATICCURRENCY) else False
        has_metal = True if set(words).intersection(METALS) else False
        has_roman = True if set(words).intersection([i[0] for i in utils.romanNumeralMap]) else False
        stop_words = set(STOPWORDS).intersection(words)
        val_swords = [STOPWORDS[w] for w in stop_words]
        val_swords.sort()
        is_question = '?' in words

        if 'WOODCHUCK' in stop_words:
            raise UnknownMaterial('I have no idea what you are talking about')

        return {'type': val_swords,
                'words': words,
                'qtd': words_qtd,
                'hcurr': has_curr,
                'hmetal': has_metal,
                'hroman': has_roman,
                'isq': is_question
                }

    def conversion(self, words):
        """
        adds or returns currency value conversion from galatic to roman to integer
        :param words: word dict of the line
        :return: integer value and used words
        """
        rnumber = []
        rw = []
        curr, rom = None, None
        for w in words:
            if w in self.gcurrency.keys():
                rnumber.append(self.gcurrency[w])
                rw.append(w)
            elif w in GALATICCURRENCY:
                curr = w
            elif w in dict(utils.romanNumeralMap).keys():
                rom = w
            if None not in (curr, rom):
                self.gcurrency[curr] = rom

        if len(rnumber) <= 0:
            return 0, rw

        return utils.fromRoman(''.join(rnumber)), rw

    def metal_price(self, words, cc=0):
        """
        returns or updates the Metal value
        :param words: wor dict
        :param cc: galatic curr int
        :return: metal value, dict words
        """

        metal, amm = None, None
        for w in words:
            if w in self.mprice.keys():
                return self.mprice[w], w

            elif w.isdigit():
                amm = int(w)
            elif w in METALS:
                metal = w
        if None not in (metal, amm):
            self.mprice[metal] = amm - cc

        return 0, ''


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Galatic Metal Prices")
    parser.add_argument("filein", help="Path of file with lines of data")
    parser.add_argument("-v", "--verbose",
                        dest="v",
                        default=False,
                        action='store_true',
                        help="Prints result")
    args = parser.parse_args()

    mgg = MGG(args.filein, args.v)
    mgg.run()
