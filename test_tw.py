import tw
import unittest

mgg = tw.MGG('./input.txt', False)
values = mgg.run()

class TWTestRight(unittest.TestCase):

    def test_right(self):
        rfile = open('./right.txt', 'r')
        rlines = [line.strip() for line in rfile.readlines()]
        rfile.close()

        self.assertEqual(values, rlines)

class TWTestValid(unittest.TestCase):
    def test_validate(self):
        rfile = open('./validate.txt', 'r')
        rlines = [line.strip() for line in rfile.readlines()]
        rfile.close()

        self.assertEqual(values, rlines)

if __name__ == '__main__':
    unittest.main()